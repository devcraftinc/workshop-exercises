from __future__ import annotations
import unittest
import math
from Pricing import *
from Interval import *
from datetime import *
from functions import *
from PricingBuilder import *

class MyTest(unittest.TestCase):
    def setUp(self):
        self.builder = PricingBuilder()

    def testGracePeriod(self):
        self.timeInParkingSpecification(justLessThan(defaultGracePeriod), 0)

    def testFirstHour(self):
        self.timeInParkingSpecification(justLessThan(defaultFirstBlockLength), defaultFirstBlockCost)

    def testIncrements(self):
        self.timeInParkingSpecification(defaultFirstBlockLength, defaultFirstBlockCost + defaultIncrementCost)
        self.timeInParkingSpecification(justLessThan(defaultFirstBlockLength + defaultIncrementLength), defaultFirstBlockCost + defaultIncrementCost)
        self.timeInParkingSpecification(defaultFirstBlockLength + defaultIncrementLength, defaultFirstBlockCost + 2 * defaultIncrementCost)

    def testWeekendEvening(self):
        self.parkingShouldCostOnDayOfWeek(
            defaultFirstWeekendDay, defaultWeekendEveningThreshold, defaultGracePeriod, defaultWeekendEveningCost)
        self.parkingShouldCostOnDayOfWeek(
            defaultFirstWeekendDay + 1, defaultWeekendEveningThreshold, defaultGracePeriod, defaultWeekendEveningCost)
        self.parkingShouldCostOnDayOfWeek(
            defaultFirstWeekendDay, justLessThan(defaultWeekendEveningThreshold), defaultGracePeriod, defaultFirstBlockCost + defaultWeekendEveningCost)

    def testMorning(self):
        self.parkingShouldCostOnDayOfWeek(0, defaultMorningThreshold, defaultGracePeriod, defaultFirstBlockCost)
        self.parkingShouldCostOnDayOfWeek(0, justLessThan(defaultMorningThreshold), defaultGracePeriod, 2 * defaultFirstBlockCost)

    def testDailyMaximum(self):
        self.timeInParkingSpecification(timedelta(hours=7), defaultDailyMaximum)

    def timeInParkingSpecification(self, timeInParking:timedelta, expectedCost:int):
        entryTime:datetime = datetime.today()
        while entryTime.weekday() >= 5:
            entryTime += timedelta(days=1)

        entryTime = midnight(entryTime) + defaultMorningThreshold
        paymentTime:datetime = entryTime + timeInParking
        self.parkingShouldCost(entryTime, paymentTime, expectedCost)

    def parkingShouldCostOnDayOfWeek(self, day:int, entryTime:timedelta, timeInParking:timedelta, expectedCost:int):
        entryDateTime = datetime.now()
        while entryDateTime.weekday() != day:
            entryDateTime += timedelta(days=1)
        entryDateTime = midnight(entryDateTime) + entryTime
        self.parkingShouldCost(entryDateTime, entryDateTime + timeInParking, expectedCost)

    def parkingShouldCost(self, entryTime:datetime, paymentTime:datetime, expectedCost:int):
        pricing = self.builder.build()
        actual = pricing.calculatePrice(Interval(entryTime, paymentTime))
        self.assertEqual(actual, expectedCost)

def justLessThan(time: timedelta):
    return time - timedelta(microseconds=1)

defaultGracePeriod:int = timedelta(minutes=10)
defaultFirstBlockLength:timedelta = timedelta(hours=1)
defaultFirstBlockCost:int = 10
defaultIncrementLength:timedelta = timedelta(minutes=15)
defaultIncrementCost:int = 3
defaultFirstWeekendDay:int = 4
defaultWeekendEveningThreshold = timedelta(hours=22)
defaultWeekendEveningCost:int = 40
defaultMorningThreshold:timedelta = timedelta(hours=6)
defaultDailyMaximum = 80
