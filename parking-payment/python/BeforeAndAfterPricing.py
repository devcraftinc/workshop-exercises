from __future__ import annotations
from functions import *
from Pricing import *
from Interval import *

class BeforeAndAfterPricing(Pricing):
    def __init__(self, before: Pricing, after: Pricing):
        self.before = before
        self.after = after

    def calculatePrice(self, timeInParking: Interval) -> int:
        timeOfDay = timedelta(hours=22)
        morning = midnight(timeInParking.start) + timedelta(hours=6)
        if timeInParking.start < morning:
            morning -= timedelta(days = 1)
        splitPoint = midnight(morning) + timeOfDay

        beforeInterval = timeInParking.endBefore(splitPoint)
        afterInterval = timeInParking.startAt(splitPoint)
        result = 0

        if beforeInterval.exists(): result += self.before.calculatePrice(beforeInterval)
        if afterInterval.exists(): result += self.after.calculatePrice(afterInterval)

        return result
