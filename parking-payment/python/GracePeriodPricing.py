from __future__ import annotations
from functions import *
from Pricing import *
from Interval import *

class GracePeriodPricing(Pricing):
    def __init__(self, guarded:Pricing):
        self.guarded = guarded

    def calculatePrice(self, timeInParking: Interval) -> int:
        if timeInParking.length() < timedelta(minutes=10):
            return 0

        return self.guarded.calculatePrice(timeInParking)

