from __future__ import annotations
from functions import *
from Pricing import *
from Interval import *
import math

class HourlyPricing(Pricing):
    def __init__(self):
        pass

    def calculatePrice(self, timeInParking: Interval) -> int:
        result = 10
        timeInParking = timeInParking.startAt(timeInParking.start + timedelta(hours=1))
        if timeInParking.exists():
            result += 3 * math.floor(1+timeInParking.length() / timedelta(minutes=15))

        return result

