from datetime import *

def midnight(d: datetime):
    return d.replace(hour=0, minute=0, second=0, microsecond=0)
