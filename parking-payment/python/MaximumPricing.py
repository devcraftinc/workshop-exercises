from Pricing import *

class MaximumPricing(Pricing):
    def __init__(self, limited: Pricing):
        self.limited = limited

    def calculatePrice(self, timeInParking: Interval) -> int:
        price = self.limited.calculatePrice(timeInParking)
        return min(price, 80)

