from __future__ import annotations
from functions import *
from datetime import *
from Pricing import *
from MaximumPricing import *
from DayOfWeekPricing import *
from FixedPricing import *
from GracePeriodPricing import *
from HourlyPricing import *
from BeforeAndAfterPricing import *
import math

class PricingBuilder:
    def build(self) -> Pricing:
        return NullPricing()

class NullPricing(Pricing):
    def calculatePrice(self, interval: Interval) -> int:
        return -1