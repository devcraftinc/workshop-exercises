from __future__ import annotations
from datetime import *

class Interval:
    def __init__(self, start:datetime, end:datetime):
        self.start = start
        self.end = end

    def length(self) -> timedelta:
        return self.end - self.start

    def startAt(self, newStart:datetime) -> Interval:
        return Interval(newStart, self.end)

    def endBefore(self, deadline:datetime) -> Interval:
        newEnd = self.end
        if deadline <= newEnd: newEnd = deadline - timedelta(microseconds=1)
        return Interval(self.start, newEnd)

    def translate(self, delta:timedelta) -> Interval:
        return Interval(self.start + delta, self.end + delta)

    def exists(self) -> bool:
        return self.end >= self.start

