from __future__ import annotations
from functions import *
from Pricing import *
from Interval import *

class DayOfWeekPricing(Pricing):
    def __init__(self, days:List[Pricing]):
        self.days = days

    def calculatePrice(self, timeInParking: Interval) -> int:
        morning = midnight(timeInParking.start) + timedelta(hours=6)
        if morning > timeInParking.start:
            morning -= timedelta(days = 1)
        result = 0
        while timeInParking.exists():
            night = morning + timedelta(days=1)
            timeToday = timeInParking.endBefore(night)
            result += self.days[timeToday.start.weekday()].calculatePrice(timeToday)
            timeInParking = timeInParking.startAt(night)
            morning = timeInParking.start

        return result
