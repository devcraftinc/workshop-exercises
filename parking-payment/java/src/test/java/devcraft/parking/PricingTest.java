//Copyright DevCraft, Inc.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

package devcraft.parking;

import static java.time.ZoneOffset.UTC;
import static java.time.temporal.ChronoUnit.DAYS;
import static java.time.temporal.ChronoUnit.HOURS;
import static java.time.temporal.ChronoUnit.MINUTES;
import static org.junit.Assert.assertEquals;

import java.time.Duration;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;

import org.junit.Before;
import org.junit.Test;

public class PricingTest {

	int gracePeriodInMinutes = 10;
	int intervalSizeInMinutes = 15;
	int intervalRate = 3;
	int initialBlockRate = 10;
	int initialBlockSizeInMinutes = 60;
	int maxDailyPayment = 80;
	int weekendNightFixedRate = 40;

	Instant mondayAt6am = asInstant("2018-01-01 06:00:00");
	Instant mondayAt10am = asInstant("2018-01-01 10:00:00");
	Instant fridayAt10pm = asInstant("2018-01-05 22:00:00");
	Instant saturdayAt10am = asInstant("2018-01-06 10:00:00");
	Instant saturdayAt10pm = asInstant("2018-01-06 22:00:00");

	Duration gracePeriod = Duration.ofMinutes(gracePeriodInMinutes);
	Duration initialBlockSize = Duration.ofMinutes(initialBlockSizeInMinutes);
	Duration intervalSize = Duration.ofMinutes(intervalSizeInMinutes);
	;
	devcraft.parking.Pricing pricing;

	@Before
	public void beforeEach() {
		pricing = new PricingBuilder().build();
	}
	
	@Test
	public void shouldPayNothingOnGracePeriod() {
		verifyExpectedPricing(0, mondayAt10am, almost(endOfGracePeriod(mondayAt10am)));
	}

	@Test
	public void shouldPayInitialBlockOnEndOfGraceTime() {
		verifyExpectedPricing(initialBlockRate, mondayAt10am, endOfGracePeriod(mondayAt10am));
	}

	@Test
	public void shouldPayForFirstIntervalOnTheInitialBlockEndMark() {
		verifyExpectedPricing(initialBlockRate + intervalRate, mondayAt10am, endOfInitialBlock(mondayAt10am));
	}

	@Test
	public void shouldPayForFirstAndSecondIntervalsOnTheFirstIntervalEndMark() {
		verifyExpectedPricing(initialBlockRate + 2 * intervalRate, mondayAt10am, endOfFirstInterval(mondayAt10am));
	}

	@Test
	public void shouldPayNightRateOnWeekendNights() {
		verifyExpectedPricing(weekendNightFixedRate, fridayAt10pm, endOfGracePeriod(fridayAt10pm));
		verifyExpectedPricing(weekendNightFixedRate, saturdayAt10pm, endOfGracePeriod(saturdayAt10pm));
	}

	@Test
	public void enterAtDayAndExitAtNight() {
		verifyExpectedPricing(weekendNightFixedRate + initialBlockRate, almost(fridayAt10pm),
				endOfGracePeriod(fridayAt10pm));
	}

	@Test
	public void maxPaymentOnWeekday() {
		verifyExpectedPricing(maxDailyPayment, mondayAt10am, mondayAt10am.plus(8, ChronoUnit.HOURS));
	}

	@Test
	public void maxPaymentOnWeekendDay() {
		verifyExpectedPricing(maxDailyPayment, saturdayAt10am, endOfGracePeriod(saturdayAt10pm));
	}

	@Test
	public void enterBeforeAndExitAfterDayStart() {
		Duration fiveMinutes = Duration.ofMinutes(5);
		verifyExpectedPricing(initialBlockRate * 2, mondayAt6am.minus(fiveMinutes), mondayAt6am.plus(5, MINUTES));
	}

	@Test
	public void parkForExactlyOneWeek() {
		Instant almostNextMondayAt6am = almost(mondayAt6am.plus(7, DAYS));
		verifyExpectedPricing(7 * maxDailyPayment, mondayAt6am, almostNextMondayAt6am);
	}

	@Test
	public void enterOnDayStartAndExitAlmostOnNextDayStart() {
		Instant almostNext6am = almost(mondayAt6am.plus(24, HOURS));
		verifyExpectedPricing(maxDailyPayment, mondayAt6am, almostNext6am);
	}

	private static Instant asInstant(String time) {
		return LocalDateTime.parse(time, DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")).toInstant(ZoneOffset.UTC);
	}

	private Instant almost(Instant instant) {
		return instant.minusMillis(1);
	}

	private Instant endOfGracePeriod(Instant instant) {
		return instant.plus(gracePeriod);
	}

	private Instant endOfFirstInterval(Instant instant) {
		return instant.plus(initialBlockSize).plus(intervalSize);
	}

	private Instant endOfInitialBlock(Instant instant) {
		return instant.plus(initialBlockSize);
	}

	private void verifyExpectedPricing(int expected, Instant entryTime, Instant paymentTime) {
		int amount = pricing.calculatePayment(entryTime, paymentTime, UTC);
		assertEquals(expected, amount);
	}

}