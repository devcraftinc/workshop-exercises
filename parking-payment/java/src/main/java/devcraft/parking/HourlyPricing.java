//Copyright DevCraft, Inc.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

package devcraft.parking;

import java.time.Duration;
import java.time.Instant;
import java.time.ZoneOffset;

class HourlyPricing implements Pricing {
	public int calculatePayment(Instant entryTime, Instant paymentTime, ZoneOffset zoneOffset) {
		Duration timeInParking = Duration.between(entryTime, paymentTime);
		if (timeInParking.toMillis() < Duration.ofHours(1).toMillis()) {
			return 10;
		}
		long remainingTime = timeInParking.toMillis() - Duration.ofHours(1).toMillis();
		return 10 + calcAdditionalPayment(remainingTime);
	}

	private int calcAdditionalPayment(long remainingTime) {
		long intervalsToPay = 1 + remainingTime / Duration.ofMinutes(15).toMillis();
		long additionalPayment = intervalsToPay * 3;
		return (int) additionalPayment;
	}
}