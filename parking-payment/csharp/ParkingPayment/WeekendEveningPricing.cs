﻿// Copyright DevCraft, Inc.
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

using System;

namespace ParkingPayment
{
  public class WeekendEveningPricing : Pricing
  {
    readonly Pricing after;
    readonly Pricing before;

    public WeekendEveningPricing(Pricing before, Pricing after)
    {
      this.before = before;
      this.after = after;
    }

    public int CalculatePrice(Interval interval)
    {
      var date = interval.EntryTime.Date;
      var morning = date + TimeSpan.FromHours(6);
      if (morning > interval.EntryTime)
        date -= TimeSpan.FromDays(1);

      var midpoint = date.Add(TimeSpan.FromHours(22));

      var afterInterval = interval.StartingAt(midpoint);
      var beforeInterval = interval.EndBefore(midpoint);

      var price = 0;

      if (beforeInterval.Exists())
        price += before.CalculatePrice(beforeInterval);

      if (afterInterval.Exists())
        price += after.CalculatePrice(afterInterval);

      return price;
    }
  }
}