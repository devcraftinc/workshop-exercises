﻿// Copyright DevCraft, Inc.
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ParkingPayment.Tests
{
  [TestClass]
  public class PricingTests
  {
    static readonly TimeSpan defaultEveningThreshold = TimeSpan.FromHours(22);
    static readonly int defaultInitialCost = 10;
    static readonly TimeSpan defaultInitialBlockLength = TimeSpan.FromHours(1);
    static readonly TimeSpan defaultGracePeriod = TimeSpan.FromMinutes(10);
    static readonly TimeSpan defaultIncrementLength = TimeSpan.FromMinutes(15);
    static readonly int defaultIncrementCost = 3;
    static readonly int defaultWeekendEveningPrice = 40;
    static readonly int defaultMaximumDailyCost = 80;
    static readonly TimeSpan defaultMorningThreshold = TimeSpan.FromHours(6);

    int actualPrice;

    DateTime entryTime;
    DateTime paymentTime;
    PricingBuilder pricingBuilder;

    [TestInitialize]
    public void Setup() => pricingBuilder = new PricingBuilder();

    [TestMethod]
    public void GracePeriod() => TimeInParkingShouldCost(JustBefore(defaultGracePeriod), 0);

    [TestMethod]
    public void FirstHour()
    {
      TimeInParkingShouldCost(defaultGracePeriod, defaultInitialCost);
      TimeInParkingShouldCost(JustBefore(defaultInitialBlockLength), defaultInitialCost);
    }

    [TestMethod]
    public void FirstIncrement()
    {
      TimeInParkingShouldCost(defaultInitialBlockLength, defaultInitialCost + defaultIncrementCost);
      TimeInParkingShouldCost(defaultInitialBlockLength + JustBefore(defaultIncrementLength), defaultInitialCost + defaultIncrementCost);
    }

    [TestMethod]
    public void NthIncrement()
    {
      TimeInParkingShouldCost(defaultInitialBlockLength + defaultIncrementLength * defaultIncrementCost, defaultInitialCost + 4 * defaultIncrementCost);
      TimeInParkingShouldCost(defaultInitialBlockLength + JustBefore(defaultIncrementLength * 4), defaultInitialCost + 4 * defaultIncrementCost);
    }

    [TestMethod]
    public void MaximumDailyPrice()
    {
      TimeInParkingShouldCost(defaultInitialBlockLength + defaultIncrementLength * 45, defaultMaximumDailyCost);
      TimeInParkingShouldCost(defaultInitialBlockLength.Add(TimeSpan.FromDays(1.5)), defaultMaximumDailyCost * 2);
      TimeInParkingShouldCost(defaultInitialBlockLength.Add(TimeSpan.FromDays(2.5)), defaultMaximumDailyCost * 3);
    }

    [TestMethod]
    public void WeekendEvening()
    {
      DayOfWeekAndTimeOfDayPrice(DayOfWeek.Friday, defaultEveningThreshold, defaultGracePeriod, defaultWeekendEveningPrice);
      DayOfWeekAndTimeOfDayPrice(DayOfWeek.Saturday, defaultEveningThreshold, defaultGracePeriod, defaultWeekendEveningPrice);
    }

    [TestMethod]
    public void WeekdayEvening()
    {
      DayOfWeekAndTimeOfDayPrice(DayOfWeek.Sunday, defaultEveningThreshold, defaultGracePeriod, defaultInitialCost);
      DayOfWeekAndTimeOfDayPrice(DayOfWeek.Monday, defaultEveningThreshold, defaultGracePeriod, defaultInitialCost);
      DayOfWeekAndTimeOfDayPrice(DayOfWeek.Tuesday, defaultEveningThreshold, defaultGracePeriod, defaultInitialCost);
      DayOfWeekAndTimeOfDayPrice(DayOfWeek.Wednesday, defaultEveningThreshold, defaultGracePeriod, defaultInitialCost);
      DayOfWeekAndTimeOfDayPrice(DayOfWeek.Thursday, defaultEveningThreshold, defaultGracePeriod, defaultInitialCost);
    }

    [TestMethod]
    public void WeekendDay()
    {
      DayOfWeekAndTimeOfDayPrice(DayOfWeek.Friday, JustBefore(defaultEveningThreshold - defaultGracePeriod), defaultGracePeriod, defaultInitialCost);
    }

    [TestMethod]
    public void StraddleWeekendDayAndNight()
    {
      DayOfWeekAndTimeOfDayPrice(DayOfWeek.Friday, JustBefore(defaultEveningThreshold), defaultGracePeriod, defaultInitialCost + defaultWeekendEveningPrice);
    }

    [TestMethod]
    public void StraddleWeekendEveningAndMorning()
    {
      DayOfWeekAndTimeOfDayPrice(DayOfWeek.Saturday, JustBefore(defaultMorningThreshold), defaultGracePeriod, defaultInitialCost + defaultWeekendEveningPrice);
    }

    [TestMethod]
    public void LongStretchIntoWeekendNightStartingBeforeEvening()
    {
      GivenEntryTime(DayOfWeek.Friday, JustBefore(defaultEveningThreshold));
      GivenPaymentTime(entryTime + TimeSpan.FromHours(4));

      WhenPriceCalculated();

      ThenPriceIs(defaultInitialCost + defaultWeekendEveningPrice);
    }

    [TestMethod]
    public void LateNightWeekendOutBeforeNewDayStarts()
    {
      GivenEntryTime(DayOfWeek.Friday, defaultEveningThreshold);
      GivenPaymentTime(entryTime + JustBefore(TimeSpan.FromHours(8)));

      WhenPriceCalculated();

      ThenPriceIs(defaultWeekendEveningPrice);
    }

    [TestMethod]
    public void BreakIntoNextMorningOfWeekend()
    {
      GivenEntryTime(DayOfWeek.Friday, defaultEveningThreshold);
      GivenPaymentTime(entryTime.AddDays(1).Date + defaultMorningThreshold);

      WhenPriceCalculated();

      ThenPriceIs(defaultInitialCost + defaultWeekendEveningPrice);
    }

    void DayOfWeekAndTimeOfDayPrice(DayOfWeek dayOfWeek, TimeSpan entryTime, TimeSpan timeInParking, int expectedPrice)
    {
      GivenEntryTime(dayOfWeek, entryTime);
      GivenPaymentTime(this.entryTime + timeInParking);

      WhenPriceCalculated();

      ThenPriceIs(expectedPrice);
    }

    void TimeInParkingShouldCost(TimeSpan timeInParking, int expectedPrice)
    {
      GivenEntryTime();
      GivenPaymentTime(entryTime + timeInParking);

      WhenPriceCalculated();

      ThenPriceIs(expectedPrice);
    }

    void GivenEntryTime() => GivenEntryTime(DayOfWeek.Monday, TimeSpan.FromHours(7));

    void GivenEntryTime(DayOfWeek dayOfWeek, TimeSpan offset)
    {
      var dateTime = DateTime.Now;
      while (dateTime.DayOfWeek != dayOfWeek)
        dateTime += TimeSpan.FromDays(1);
      entryTime = dateTime.Date + offset;
    }

    void GivenPaymentTime(DateTime paymentTime) => this.paymentTime = paymentTime;

    void WhenPriceCalculated() => actualPrice = pricingBuilder.Build().CalculatePrice(new Interval(entryTime, paymentTime));

    void ThenPriceIs(int expectedPrice) => Assert.AreEqual(expectedPrice, actualPrice);

    static TimeSpan JustBefore(TimeSpan ts) => ts - TimeSpan.FromTicks(1);
  }
}