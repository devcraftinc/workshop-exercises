import { Pricing } from "./Pricing";
import { Moment } from "moment";
import * as moment from "moment";

export class HourlyPricing implements Pricing {
    public calculatePayment(entryTime: Moment, paymentTime: Moment): number {
        const splitMark = moment(entryTime).add(moment.duration(60, "minutes"));

        let result: number = 0;
        result += 10;

        if (paymentTime.isSameOrAfter(splitMark)) {
            entryTime = moment.max(splitMark, entryTime);
            const totalTime = paymentTime.diff(entryTime, "milliseconds");
            let intervalInMillis = moment.duration(15, "minutes").asMilliseconds();
            let numberOfIncrements = 1 + Math.floor((totalTime / intervalInMillis));
            result += numberOfIncrements * 3;
        }
        
        return result;
    }
}