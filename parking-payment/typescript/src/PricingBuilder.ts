//Copyright DevCraft, Inc.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

import * as moment from "moment";
import {duration} from "moment";
import {LimitedPricing} from "./LimitedPricing";
import {SplitPricing} from "./SplitPricing";
import {FixedPricing} from "./FixedPricing";
import {Pricing} from "./Pricing";
import {GracePeriodPricing} from "./GracePeriodPricing";
import {DayOfWeekPricing} from "./DayOfWeekPricing";
import {HourlyPricing} from "./HourlyPricing";
import {Moment} from "moment";

class NullPricing implements Pricing {
    public calculatePayment(entryTime: moment.Moment, paymentTime: moment.Moment): number {
        return -1;
    }
}

export class PricingBuilder {
    public build(): Pricing {
        return new NullPricing();
    }
}