//Copyright DevCraft, Inc.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

import {Pricing} from "./Pricing";
import * as moment from "moment";
import {duration} from "moment";

export class DayOfWeekPricing implements Pricing {
    private readonly dailyPricing: Pricing[] = [null, null, null, null, null, null, null];

    constructor(
        sun: Pricing,
        mon: Pricing,
        tue: Pricing,
        wed: Pricing,
        thu: Pricing,
        fri: Pricing,
        sat: Pricing) {

        this.dailyPricing[0] = sun;
        this.dailyPricing[1] = mon;
        this.dailyPricing[2] = tue;
        this.dailyPricing[3] = wed;
        this.dailyPricing[4] = thu;
        this.dailyPricing[5] = fri;
        this.dailyPricing[6] = sat;
    }

    calculatePayment(entryTime: moment.Moment, paymentTime: moment.Moment): number {
        let splitMark = this.nextDayStart(entryTime);
        let result = 0;
        while (paymentTime.isSameOrAfter(splitMark)) {
            let dailyPricing = this.getDailyPricing(entryTime);
            result += dailyPricing.calculatePayment(entryTime, splitMark);
            entryTime = moment(splitMark);
            splitMark.add(24, "hours");
        }
        let dailyPricing = this.getDailyPricing(entryTime);
        result += dailyPricing.calculatePayment(entryTime, paymentTime);
        return result;
    }

    private getDailyPricing(entryTime: moment.Moment) {
        let weekday = this.nextDayStart(entryTime).isoWeekday() - 1;
        let dailyPricing = this.dailyPricing[weekday];
        return dailyPricing;
    }

    private nextDayStart(entryTime: moment.Moment) {
        let splitMark = moment(entryTime).startOf("day").add(duration(6, "hours"));
        if (splitMark.isSameOrBefore(entryTime))
            splitMark.add(24, "hours");
        return splitMark;
    }
}