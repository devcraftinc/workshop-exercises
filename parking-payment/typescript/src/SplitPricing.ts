//Copyright DevCraft, Inc.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

import * as moment from "moment";
import {Pricing} from "./Pricing";

export class SplitPricing implements Pricing {
    constructor(private firstPricing: Pricing, private secondPricing: Pricing) {
    }

    public calculatePayment(entryTime: moment.Moment, paymentTime: moment.Moment): number {
        var splitMark = moment(entryTime).startOf("day").add(moment.duration(22, "hours"));
        if (entryTime.isBefore(moment(entryTime).startOf("day").add(moment.duration(6, "hours"))))
            splitMark.subtract(1, 'days');

        let result: number = 0;
        if (entryTime.isBefore(splitMark))
            result += this.firstPricing.calculatePayment(entryTime, moment.min(paymentTime, splitMark));

        if (paymentTime.isSameOrAfter(splitMark))
            result += this.secondPricing.calculatePayment(moment.max(splitMark, entryTime), paymentTime);

        return result;
    }
}