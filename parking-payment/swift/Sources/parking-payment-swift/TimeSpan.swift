// Copyright 2018 DevCraft Inc.
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

import Foundation

public struct TimeSpan {
    private static let secondsInMinute : Int = 60
    private static let minutesInHour : Int = 60
    private static let hoursInDay : Int = 24
    private static let nanosecondsInSecond : Int = 1000000000

    public let ticks : TimeInterval

    public static let zero = TimeSpan(ticks: 0)

    public static func from(days : Double) -> TimeSpan {
        return from(hours: days * Double(TimeSpan.hoursInDay))
    }
    
    public static func from(hours : Double) -> TimeSpan {
        return from(minutes: hours * Double(TimeSpan.minutesInHour))
    }

    public static func from(minutes: Double) -> TimeSpan {
        return TimeSpan(ticks: minutes * Double(TimeSpan.secondsInMinute))
    }

    public static func from(ticks : Double) -> TimeSpan {
        return TimeSpan(ticks: ticks)
    }

    public var days : Int {
        return Int(ticks / Double(TimeSpan.hoursInDay * TimeSpan.minutesInHour * TimeSpan.secondsInMinute))
    }

    public var hours : Int {
        return Int(ticks / Double(TimeSpan.minutesInHour * TimeSpan.secondsInMinute)) % TimeSpan.hoursInDay
    }

    public var minutes : Int {
        return Int(ticks / Double(TimeSpan.secondsInMinute)) % TimeSpan.minutesInHour
    }

    public var seconds : Int {
        return Int(ticks) % TimeSpan.secondsInMinute
    }

    public var nanoseconds : Int {
        return Int(ticks.truncatingRemainder(dividingBy: 1) * Double(TimeSpan.nanosecondsInSecond))
    }
}

public func +(left: TimeSpan, right:TimeSpan) -> TimeSpan {
    return TimeSpan(ticks: left.ticks + right.ticks)
}

public func -(left: TimeSpan, right: TimeSpan) -> TimeSpan {
    return TimeSpan(ticks: left.ticks - right.ticks)
}

public func <(left: TimeSpan, right: TimeSpan) -> Bool {
    return left.ticks < right.ticks
}

public func *(left: TimeSpan, right: Double) -> TimeSpan {
    return TimeSpan(ticks: left.ticks * right)
}

public func *(left: TimeSpan, right: Int) -> TimeSpan {
    return TimeSpan(ticks: left.ticks * Double(right))
}

public func +(left: Date, right: TimeSpan) -> Date {
    return left + right.ticks
}