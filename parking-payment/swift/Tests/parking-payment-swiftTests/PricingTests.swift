﻿// Copyright 2018 DevCraft Inc.
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

import Foundation
import XCTest
import parking_payment_swift

public class PricingTests : XCTestCase {
  let defaultEveningThreshold : TimeSpan = TimeSpan.from(hours: 22)
  let defaultInitialCost : Int = 10
  let defaultInitialBlockLength : TimeSpan = TimeSpan.from(hours: 1)
  let defaultGracePeriod : TimeSpan = TimeSpan.from(minutes: 10)
  let defaultIncrementLength : TimeSpan = TimeSpan.from(minutes: 15)
  let defaultIncrementCost : Int = 3
  let defaultWeekendEveningPrice : Int = 40
  let defaultMaximumDailyCost : Int = 80
  let defaultMorningThreshold : TimeSpan = TimeSpan.from(hours: 6)

  var actualPrice : Int = 0
  var entryTime : Date = Date()
  var paymentTime : Date = Date()
  var pricingBuilder : PricingBuilder = PricingBuilder()

  public override func setUp() {
    pricingBuilder = PricingBuilder()
  }

  static var allTests = [
    ("gracePeriod", testGracePeriod),
    ("testFirstHour", testFirstHour),
    ("testFirstIncrement", testFirstIncrement),
    ("testNthIncrement", testNthIncrement),
    ("testMaximumDailyPrice", testMaximumDailyPrice),
    ("testWeekendEvening", testWeekendEvening),
    ("testWeekdayEvening", testWeekdayEvening),
    ("testWeekendDay", testWeekendDay),
    ("testStraddleWeekendDayAndNight", testStraddleWeekendDayAndNight),
    ("testLongStretchIntoWeekendNightStartingBeforeEvening", testLongStretchIntoWeekendNightStartingBeforeEvening),
    ("testLateNightWeekendOutBeforeNewDayStarts", testLateNightWeekendOutBeforeNewDayStarts),
    ("testDailyMaximumAutomaticallyIncludesWeekendEvenings", testDailyMaximumAutomaticallyIncludesWeekendEvenings)
  ]

  public func testGracePeriod() {
    timeInParkingShouldCost(timeInParking: justBefore(defaultGracePeriod), expectedPrice: 0)
  }

  public func testFirstHour() {
    timeInParkingShouldCost(timeInParking: defaultGracePeriod, expectedPrice: defaultInitialCost)
    timeInParkingShouldCost(timeInParking: justBefore(defaultInitialBlockLength), expectedPrice: defaultInitialCost)
  }

  public func testFirstIncrement() {
    timeInParkingShouldCost(timeInParking: defaultInitialBlockLength, expectedPrice: defaultInitialCost + defaultIncrementCost)
    timeInParkingShouldCost(timeInParking: defaultInitialBlockLength + justBefore(defaultIncrementLength), expectedPrice: defaultInitialCost + defaultIncrementCost)
  }

  public func testNthIncrement() {
    timeInParkingShouldCost(timeInParking: defaultInitialBlockLength + defaultIncrementLength * defaultIncrementCost, expectedPrice: defaultInitialCost + 4 * defaultIncrementCost)
    timeInParkingShouldCost(timeInParking: defaultInitialBlockLength + justBefore(defaultIncrementLength * 4), expectedPrice: defaultInitialCost + 4 * defaultIncrementCost)
  }

  public func testMaximumDailyPrice() {
    timeInParkingShouldCost(timeInParking: defaultInitialBlockLength + defaultIncrementLength * 45, expectedPrice: defaultMaximumDailyCost)
    timeInParkingShouldCost(timeInParking: defaultInitialBlockLength + TimeSpan.from(days: 1.5), expectedPrice: defaultMaximumDailyCost * 2)
    timeInParkingShouldCost(timeInParking: defaultInitialBlockLength + TimeSpan.from(days: 2.5), expectedPrice: defaultMaximumDailyCost * 3)
  }

  public func testWeekendEvening() {
    dayOfWeekAndTimeOfDayPrice(dayOfWeek: .friday, entryTime: defaultEveningThreshold, timeInParking: defaultGracePeriod, expectedPrice: defaultWeekendEveningPrice)
    dayOfWeekAndTimeOfDayPrice(dayOfWeek: .saturday, entryTime: defaultEveningThreshold, timeInParking: defaultGracePeriod, expectedPrice: defaultWeekendEveningPrice)
  }

  public func testWeekdayEvening() {
    dayOfWeekAndTimeOfDayPrice(dayOfWeek: .sunday, entryTime: defaultEveningThreshold, timeInParking: defaultGracePeriod, expectedPrice: defaultInitialCost)
    dayOfWeekAndTimeOfDayPrice(dayOfWeek: .monday, entryTime: defaultEveningThreshold, timeInParking: defaultGracePeriod, expectedPrice: defaultInitialCost)
    dayOfWeekAndTimeOfDayPrice(dayOfWeek: .tuesday, entryTime: defaultEveningThreshold, timeInParking: defaultGracePeriod, expectedPrice: defaultInitialCost)
    dayOfWeekAndTimeOfDayPrice(dayOfWeek: .wednesday, entryTime: defaultEveningThreshold, timeInParking: defaultGracePeriod, expectedPrice: defaultInitialCost)
    dayOfWeekAndTimeOfDayPrice(dayOfWeek: .thursday, entryTime: defaultEveningThreshold, timeInParking: defaultGracePeriod, expectedPrice: defaultInitialCost)
  }

  public func testWeekendDay() {
    dayOfWeekAndTimeOfDayPrice(dayOfWeek: .friday, entryTime: justBefore(defaultEveningThreshold - defaultGracePeriod), timeInParking: defaultGracePeriod, expectedPrice: defaultInitialCost)
  }

  public func testStraddleWeekendDayAndNight() {
    dayOfWeekAndTimeOfDayPrice(dayOfWeek: .friday, entryTime: justBefore(defaultEveningThreshold), timeInParking: defaultGracePeriod, expectedPrice: defaultInitialCost + defaultWeekendEveningPrice)
  }

  public func testLongStretchIntoWeekendNightStartingBeforeEvening() {
    givenEntryTime(.friday, justBefore(defaultEveningThreshold))
    givenPaymentTime(entryTime + TimeSpan.from(hours: 4))

    whenPriceCalculated()

    thenPriceIs(defaultInitialCost + defaultWeekendEveningPrice)
  }

  public func testLateNightWeekendOutBeforeNewDayStarts() {
    givenEntryTime(.friday, defaultEveningThreshold)
    givenPaymentTime(entryTime + justBefore(TimeSpan.from(hours: 8)))

    whenPriceCalculated()

    thenPriceIs(defaultWeekendEveningPrice)
  }
  
  public func testDailyMaximumAutomaticallyIncludesWeekendEvenings() {
    dayOfWeekAndTimeOfDayPrice(dayOfWeek: .friday, entryTime: TimeSpan.from(hours: 12), timeInParking: TimeSpan.from(hours: 11), expectedPrice: defaultMaximumDailyCost)
  }

  private func crossMorningThresholdPricing(dayOfWeek : DayOfWeek, morning : TimeSpan, day1Price : Int, day2Price : Int) {
    dayOfWeekAndTimeOfDayPrice(dayOfWeek: dayOfWeek, entryTime: justBefore(morning), timeInParking: defaultGracePeriod, expectedPrice: day1Price + day2Price)
  }

  private func dayOfWeekAndTimeOfDayPrice(dayOfWeek : DayOfWeek, entryTime : TimeSpan, timeInParking : TimeSpan, expectedPrice : Int) {
    givenEntryTime(dayOfWeek, entryTime)
    givenPaymentTime(self.entryTime + timeInParking)

    whenPriceCalculated()

    thenPriceIs(expectedPrice)
  }

  private func timeInParkingShouldCost(timeInParking: TimeSpan, expectedPrice: Int) {
    givenEntryTime()
    givenPaymentTime(entryTime + timeInParking)

    whenPriceCalculated()

    thenPriceIs(expectedPrice)
  }

  private func givenEntryTime()  {
    givenEntryTime(.monday, TimeSpan.from(hours:7))
  }

  private func givenEntryTime(_ dayOfWeek : DayOfWeek, _ offset : TimeSpan) {
    var dateTime = Calendar.current.startOfDay(for:Date())
    while (getDayOfWeek(from:dateTime) != dayOfWeek) {
      dateTime = addDays(value: 1, to: dateTime)
    }
    entryTime = changeTimeOfDay(from: dateTime, to:offset)
  }

  private func givenPaymentTime(_ paymentTime : Date) {
    self.paymentTime = paymentTime
  }

  private func whenPriceCalculated() {
    actualPrice = pricingBuilder.build().calculatePrice(for: Interval(start: entryTime, end: paymentTime))
  }

  private func thenPriceIs(_ expectedPrice: Int) {
    XCTAssertEqual(actualPrice, expectedPrice)
  }

  private func justBefore(_ ts: TimeSpan) -> TimeSpan {
    return ts - TimeSpan.from(ticks: 1)
  }
}
