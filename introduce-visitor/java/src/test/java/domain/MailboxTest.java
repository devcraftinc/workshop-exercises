package domain;

import static org.mockito.Mockito.mock;

import org.junit.Assert;
import org.junit.Test;

public class MailboxTest {

	@Test
	public void canIterateMessageAfterAdd() {
		Message m = mock(Message.class);
		Mailbox mbx = new Mailbox("MBX");
		mbx.add(m);
		Assert.assertEquals(m, mbx.iterator().next());
	}

	@Test
	public void canDeleteMessgaeByIdAfterAdd() {
		Message m = mock(Message.class);
		Mailbox mbx = new Mailbox("MBX");
		mbx.add(m);
		mbx.delete(0);
		Assert.assertFalse(mbx.iterator().hasNext());
	}

	@Test
	public void canDeleteMessgaeAfterAdd() {
		Message m = mock(Message.class);
		Mailbox mbx = new Mailbox("MBX");
		mbx.add(m);
		mbx.delete(m);
		Assert.assertFalse(mbx.iterator().hasNext());
	}
}