package ui;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;

import domain.AddressBook;
import domain.Email;
import domain.Mailbox;
import domain.Text;
import domain.Voice;

@RunWith(org.mockito.junit.MockitoJUnitRunner.class)
public class ListMailboxInteractionTest {

	@Mock
	AddressBook addressBook;

	@Mock
	Console console;

	Mailbox mbx = new Mailbox("mbx");
	Email email = new Email();
	Text text = new Text();
	Voice voice = new Voice();

	@Before
	public void setup() {
		text.setText("content");
		email.setSubject("subj");
		voice.setOrigin("sender");
	}

	@Test
	public void verifyShowInvalidMbxAddressMsgOnBadInputAddress() {
		when(console.readLine("Enter mailbox address:")).thenReturn("invalid address");
		when(addressBook.getMailbox("invalid address")).thenReturn(null);
		ListMailboxInteraction listAction = new ListMailboxInteraction(console, addressBook);
		listAction.listMailbox();
		verify(console).show("invalid mailbox address");
	}

	@Test
	public void verifyShowMailboxOnValidInputAddress() {
		when(console.readLine("Enter mailbox address:")).thenReturn("mbx");
		when(addressBook.getMailbox("mbx")).thenReturn(mbx);
		ListMailboxInteraction listAction = new ListMailboxInteraction(console, addressBook);
		listAction.listMailbox();
		verify(console).show("Mailbox mbx is empty");
	}

	@Test
	public void showMessagesByType() {
		when(console.readLine("Enter mailbox address:")).thenReturn("mbx");
		when(addressBook.getMailbox("mbx")).thenReturn(mbx);

		mbx.add(email);
		mbx.add(text);
		mbx.add(voice);

		ListMailboxInteraction listAction = new ListMailboxInteraction(console, addressBook);
		listAction.listMailbox();

		verify(console).show("Content of mailbox mbx:");
		verify(console).show("1: Email: subj");
		verify(console).show("2: Text: content");
		verify(console).show("3: Voice: from sender");
	}

}