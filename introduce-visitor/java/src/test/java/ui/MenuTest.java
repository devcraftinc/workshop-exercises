package ui;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.Test;
import org.mockito.Mockito;

public class MenuTest {
	
	@Test
	public void showMenu() {
		Console console = mock(Console.class);
		MenuItem item =  mock(MenuItem.class);
		when(item.getName()).thenReturn("item1");
		Menu m = new Menu(console,"title");
		m.addItem(item);
		m.show();
		verify(console).show("title");
		verify(console).show("1. item1");
		verify(console).readLine("Select Option:");			
	}

	@Test
	public void invalidOptionSelection() {
		Console console = mock(Console.class);
		Menu m = new Menu(console,"title");
		when(console.readLine("Select Option:")).thenReturn("1");			
		m.show();
		verify(console).show("Invalid Option !!");			
	}

	@Test
	public void validOptionSelection() {
		Console console = mock(Console.class);
		MenuItem item =  mock(MenuItem.class);
		when(item.getName()).thenReturn("item1");
		Menu m = new Menu(console,"title");
		m.addItem(item);
		when(console.readLine("Select Option:")).thenReturn("1");			
		m.show();
		verify(item).click();			
	}
	
}