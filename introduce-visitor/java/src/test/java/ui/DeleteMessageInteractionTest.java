package ui;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;

import domain.AddressBook;
import domain.Mailbox;

@RunWith(org.mockito.junit.MockitoJUnitRunner.class)
public class DeleteMessageInteractionTest {

	@Mock
	AddressBook addressBook;

	@Mock
	Console console;

	@Mock
	Mailbox mbx;

	@Test
	public void verifyInvalidAddressMessageOnBadMbxAddress() {
		when(console.readLine("Delete message from Mailbox:")).thenReturn("invalid");
		when(addressBook.getMailbox("invalid")).thenReturn(null);
		DeleteMessageInteraction uiAction = new DeleteMessageInteraction(console, addressBook);
		uiAction.deleteMessage();
		verify(console).show("invalid is not a valid address !");
	}

	@Test
	public void verifyDeleteMessageOnValidAddress() {
		when(console.readLine("Delete message from Mailbox:")).thenReturn("valid");
		when(addressBook.getMailbox("valid")).thenReturn(mbx);
		when(console.readLine("Enter message id:")).thenReturn("1");
		DeleteMessageInteraction uiAction = new DeleteMessageInteraction(console, addressBook);
		uiAction.deleteMessage();
		verify(mbx).delete(0);
	}
}