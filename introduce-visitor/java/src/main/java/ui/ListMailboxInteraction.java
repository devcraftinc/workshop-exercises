package ui;

import domain.AddressBook;
import domain.Email;
import domain.Mailbox;
import domain.Message;
import domain.Text;
import domain.Voice;

public class ListMailboxInteraction {
	private AddressBook addressBook;
	private final Console console;

	public ListMailboxInteraction(Console console, AddressBook addressBook) {
		this.console = console;
		this.addressBook = addressBook;
	}

	public void listMailbox() {
		String address = console.readLine("Enter mailbox address:");
		Mailbox mbx = addressBook.getMailbox(address.toLowerCase());
		if (mbx == null) {
			console.show("invalid mailbox address");
			return;
		}
		this.presetMailbox(mbx);
	}

	private void presetMailbox(Mailbox mailbox) {
		if (mailbox.count() == 0) {
			console.show("Mailbox " + mailbox.getAddress() + " is empty");
			return;
		}

		console.show("Content of mailbox " + mailbox.getAddress() + ":");
		int msgNum = 0;
		for (Message m : mailbox) {
			msgNum++;
			String title = null;
			if (m instanceof Email) {
				Email msg = (Email) m;
				title = "Email: " + msg.getSubject();
			}
			if (m instanceof Text) {
				Text msg = (Text) m;
				title = "Text: " + msg.getText();
			}
			if (m instanceof Voice) {
				Voice msg = (Voice) m;
				title = "Voice: from " + msg.getOrigin();
			}
			title = msgNum + ": " + title;
			console.show(title);
		}
	}
}