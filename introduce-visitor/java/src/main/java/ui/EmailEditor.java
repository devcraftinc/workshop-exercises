package ui;

import domain.AddressBook;
import domain.Email;

public class EmailEditor {

	private AddressBook addressBook;

	public EmailEditor(AddressBook addressBook) {
		this.addressBook = addressBook;
	}

	public boolean edit(Email msg) {
		Console console = new Console();
		if (!MessageEditingUtils.editAddresses(msg, addressBook, console))
			return false;
		readSubject(msg, console);
		readContent(msg, console);
		return true;
	}

	private void readContent(Email msg, Console console) {
		String messageContent = console.readLine("Enter email's content:");
		if (!messageContent.trim().equals("")) {
			msg.setContent(messageContent);
		}
	}

	private void readSubject(Email msg, Console console) {
		String messageSubject = console.readLine("Enter email's subject:");
		if (!messageSubject.trim().equals("")) {
			msg.setSubject(messageSubject);
		}
	}

}
