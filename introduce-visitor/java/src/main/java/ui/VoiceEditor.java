package ui;

import domain.AddressBook;
import domain.Voice;

public class VoiceEditor {

	private AddressBook addressBook;

	public VoiceEditor(AddressBook addressBook) {
		this.addressBook = addressBook;
	}

	public boolean edit(Voice msg) {
		Console console = new Console();
		if (!MessageEditingUtils.editAddresses(msg, addressBook, console))
			return false;
		readText(msg, console);
		return true;
	}

	private void readText(Voice msg, Console console) {
		String voicePayload = console.readLine("Speak your message:");
		if (!voicePayload.trim().equals("")) {
			msg.setPayload(voicePayload);
		}
	}

}