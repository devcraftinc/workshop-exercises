package domain;

public class Text extends Message {
	private String text;

	public void setText(String content) {
		this.text = content;
	}

	public String getText() {
		return text;
	}

}
