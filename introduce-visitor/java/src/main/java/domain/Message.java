package domain;
public abstract class Message {

	private String destination;

	private String origin;

	public String getDestination() {
		return destination;
	}

	public String getOrigin() {
		return origin;
	}

	public void setDestination(String address) {
		destination = address;
	}

	public void setOrigin(String address) {
		origin = address;
	}

}