package domain;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Mailbox implements Iterable<Message> {

	private String address;

	private List<Message> messages;

	public Mailbox(String address) {
		this.address = address;
		messages = new ArrayList<Message>();
	}

	public void add(Message msg) {
		messages.add(msg);
	}

	public Message delete(int msgId) {
		Message m = (Message) messages.get(msgId);
		messages.remove(msgId);
		return m;
	}

	public void delete(Message msg) {
		messages.remove(msg);
	}

	public String getAddress() {
		return address;
	}

	public int count() {
		return messages.size();
	}

	@Override
	public Iterator<Message> iterator() {
		return messages.iterator();
	}
}