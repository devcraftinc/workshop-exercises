// Copyright 2018 DevCraft Inc.
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

using System;
using System.Collections.Generic;
using System.Linq;

namespace IntroduceVisitor
{
  public class Menu
  {
    readonly Console console;
    readonly List<MenuItem> items;
    readonly string name;

    public Menu(Console console, string name)
    {
      this.console = console;
      items = new List<MenuItem>();
      this.name = name;
    }

    public void AddItem(MenuItem item)
    {
      items.Add(item);
    }

    public void Show()
    {
      console.Show(name);
      foreach (var placement in items.Select((item, index) => new {item, index}))
        console.Show((placement.index + 1) + ". " + placement.item.GetName());

      try
      {
        var option = console.ReadLine("Select Option:");
        if (!int.TryParse(option, out var index))
          return;
        var item = items[index - 1];
        item.Click();
      }
      catch (Exception)
      {
        console.Show("Invalid Option !!");
      }
    }
  }
}