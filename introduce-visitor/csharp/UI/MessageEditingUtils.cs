// Copyright 2018 DevCraft Inc.
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

namespace IntroduceVisitor
{
  public class MessageEditingUtils
  {
    public static bool EditAddresses(Message msg, AddressBook addressBook,
      Console console)
    {
      var senderAddress = console.ReadLine("Enter sender's address:");
      if (!senderAddress.Trim().Equals(""))
      {
        if (addressBook.GetMailbox(senderAddress.ToLower()) == null)
        {
          console.Show("invalid sender mailbox");
          return false;
        }

        msg.Origin = senderAddress;
      }

      var destinationAddress = console.ReadLine("Enter destination address:");
      if (destinationAddress.Trim() != "")
      {
        var destinationMailbox = addressBook.GetMailbox(destinationAddress.ToLower());
        if (destinationMailbox == null)
        {
          console.Show("invalid destination mailbox");
          return false;
        }

        msg.Destination = destinationAddress;
      }

      return true;
    }
  }
}