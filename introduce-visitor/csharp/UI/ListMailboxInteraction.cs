// Copyright 2018 DevCraft Inc.
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

using System.Linq;

namespace IntroduceVisitor
{
  public class ListMailboxInteraction
  {
    readonly AddressBook addressBook;
    readonly Console console;

    public ListMailboxInteraction(Console console, AddressBook addressBook)
    {
      this.console = console;
      this.addressBook = addressBook;
    }

    public void ListMailbox()
    {
      var address = console.ReadLine("Enter mailbox address:");
      var mbx = addressBook.GetMailbox(address.ToLower());
      if (mbx == null)
      {
        console.Show("invalid mailbox address");
        return;
      }

      PresetMailbox(mbx);
    }

    void PresetMailbox(Mailbox mailbox)
    {
      if (!mailbox.Any())
      {
        console.Show("Mailbox " + mailbox.Address + " is empty");
        return;
      }

      console.Show("Content of mailbox " + mailbox.Address + ":");
      var msgNum = 0;
      foreach (var m in mailbox)
      {
        msgNum++;
        string title = null;
        if (m is Email email)
          title = "Email: " + email.Subject;

        if (m is Text text)
          title = "Text: " + text.Content;

        if (m is Voice voice)
          title = "Voice: from " + voice.Origin;

        title = msgNum + ": " + title;
        console.Show(title);
      }
    }
  }
}