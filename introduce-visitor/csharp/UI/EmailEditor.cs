// Copyright 2018 DevCraft Inc.
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

namespace IntroduceVisitor
{
  public class EmailEditor
  {
    readonly AddressBook addressBook;

    public EmailEditor(AddressBook addressBook)
    {
      this.addressBook = addressBook;
    }

    public bool Edit(Email msg)
    {
      var console = new Console();
      if (!MessageEditingUtils.EditAddresses(msg, addressBook, console))
        return false;
      ReadSubject(msg, console);
      ReadContent(msg, console);
      return true;
    }

    static void ReadContent(Email msg, Console console)
    {
      var messageContent = console.ReadLine("Enter email's content:");
      if (messageContent.Trim() != "") msg.Content = messageContent;
    }

    static void ReadSubject(Email msg, Console console)
    {
      var messageSubject = console.ReadLine("Enter email's subject:");
      if (messageSubject.Trim() != "") msg.Subject = messageSubject;
    }
  }
}