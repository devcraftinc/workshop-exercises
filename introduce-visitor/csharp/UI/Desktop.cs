// Copyright 2018 DevCraft Inc.
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

namespace IntroduceVisitor
{
  public class Desktop
  {
    sealed class SendMenuItem : MenuItem
    {
      readonly Desktop outer;

      public SendMenuItem(Desktop outer) : base("Send Message")
      {
        this.outer = outer;
      }

      public override void Click()
      {
        outer.sendMessageUIAction.SendMessage();
      }
    }

    sealed class ListMessagesMenuItem : MenuItem
    {
      readonly Desktop outer;

      public ListMessagesMenuItem(Desktop outer) : base("List Messages")
      {
        this.outer = outer;
      }

      public override void Click()
      {
        outer.listMailboxUIAction.ListMailbox();
      }
    }

    sealed class DeleteMessageMenuItem : MenuItem
    {
      readonly Desktop outer;

      public DeleteMessageMenuItem(Desktop outer) : base("Delete Message")
      {
        this.outer = outer;
      }

      public override void Click()
      {
        outer.deleteMessageUIAction.DeleteMessage();
      }
    }

    sealed class ExitMenuItem : MenuItem
    {
      readonly Desktop outer;

      public ExitMenuItem(Desktop outer) : base("Exit")
      {
        this.outer = outer;
      }

      public override void Click()
      {
        outer.application.Stop();
      }
    }

    readonly Application application;
    readonly Console console = new Console();
    readonly DeleteMessageInteraction deleteMessageUIAction;
    readonly ListMailboxInteraction listMailboxUIAction;
    readonly SendMessageInteraction sendMessageUIAction;

    public Desktop(Application app)
    {
      application = app;
      listMailboxUIAction = new ListMailboxInteraction(console, app.GetAddressBook());
      sendMessageUIAction = new SendMessageInteraction(console, app.GetAddressBook());
      deleteMessageUIAction = new DeleteMessageInteraction(console, app.GetAddressBook());
    }

    public void Start()
    {
      var mainMenu = new Menu(console, "Select an option:");

      mainMenu.AddItem(new SendMenuItem(this));
      mainMenu.AddItem(new DeleteMessageMenuItem(this));
      mainMenu.AddItem(new ListMessagesMenuItem(this));
      mainMenu.AddItem(new ExitMenuItem(this));

      while (true) mainMenu.Show();
    }
  }
}