// Copyright 2018 DevCraft Inc.
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace IntroduceVisitor
{
  [TestClass]
  public class MenuTest
  {
    [TestMethod]
    public void ShowMenu()
    {
      var console = Mock.Of<Console>();
      var item = Mock.Of<MenuItem>();
      Mock.Get(item).Setup(i => i.GetName()).Returns("item1");
      var m = new Menu(console, "title");
      m.AddItem(item);
      m.Show();
      var consoleController = Mock.Get(console);
      consoleController.Verify(c => c.Show("title"));
      consoleController.Verify(c => c.Show("1. item1"));
      consoleController.Verify(c => c.ReadLine("Select Option:"));
    }

    [TestMethod]
    public void InvalidOptionSelection()
    {
      var console = Mock.Of<Console>();
      var m = new Menu(console, "title");
      Mock.Get(console).Setup(c => c.ReadLine("Select Option:")).Returns("1");
      m.Show();
      var consoleController = Mock.Get(console);
      consoleController.Verify(c => c.Show("Invalid Option !!"));
    }

    [TestMethod]
    public void ValidOptionSelection()
    {
      var console = Mock.Of<Console>();
      var item = Mock.Of<MenuItem>();
      var itemController = Mock.Get(item);
      itemController.Setup(i => i.GetName()).Returns("item1");
      var m = new Menu(console, "title");
      m.AddItem(item);
      Mock.Get(console).Setup(c => c.ReadLine("Select Option:")).Returns("1");
      m.Show();
      itemController.Verify(i => i.Click());
    }
  }
}