// Copyright 2018 DevCraft Inc.
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

using System;
using System.Linq.Expressions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace IntroduceVisitor
{
  [TestClass]
  public class DeleteMessageInteractionTest
  {
    AddressBook addressBook;
    Console console;
    Mailbox mbx;

    [TestInitialize]
    public void Setup()
    {
      addressBook = Mock.Of<AddressBook>();
      console = Mock.Of<Console>();
      mbx = Mock.Of<Mailbox>();
    }

    [TestMethod]
    public void VerifyInvalidAddressMessageOnBadMbxAddress()
    {
      Mock.Get(console).Setup(c => c.ReadLine("Delete message from Mailbox:")).Returns("invalid");
      Mock.Get(addressBook).Setup(a => a.GetMailbox("invalid")).Returns((Mailbox) null);
      var uiAction = new DeleteMessageInteraction(console, addressBook);
      uiAction.DeleteMessage();
      Mock.Get(console).Verify(c => c.Show("invalid is not a valid address !"));
    }

    [TestMethod]
    public void VerifyDeleteMessageOnValidAddress()
    {
      Mock.Get(console).Setup(c => c.ReadLine("Delete message from Mailbox:")).Returns("valid");
      Mock.Get(addressBook).Setup(a => a.GetMailbox("valid")).Returns(mbx);
      Mock.Get(console).Setup(c => c.ReadLine("Enter message id:")).Returns("1");
      var uiAction = new DeleteMessageInteraction(console, addressBook);
      uiAction.DeleteMessage();
      Mock.Get(mbx).Verify((Expression<Action<Mailbox>>) (m => m.Delete(0)));
    }
  }
}