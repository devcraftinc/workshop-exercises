// Copyright 2018 DevCraft Inc.
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace IntroduceVisitor
{
  [TestClass]
  public class ListMailboxInteractionTest
  {
    AddressBook addressBook;

    Console console;
    Email email;

    Mailbox mbx;
    Text text;
    Voice voice;

    [TestInitialize]
    public void Setup()
    {
      mbx = new Mailbox("mbx");
      email = new Email();
      text = new Text();
      voice = new Voice();
      text.Content = "content";
      email.Subject = "subj";
      voice.Origin = "sender";
      console = Mock.Of<Console>();
      addressBook = Mock.Of<AddressBook>();
    }

    [TestMethod]
    public void VerifyShowInvalidMbxAddressMsgOnBadInputAddress()
    {
      Mock.Get(console).Setup(c => c.ReadLine("Enter mailbox address:")).Returns("invalid address");
      Mock.Get(addressBook).Setup(a => a.GetMailbox("invalid address")).Returns((Mailbox) null);
      var listAction = new ListMailboxInteraction(console, addressBook);
      listAction.ListMailbox();
      Mock.Get(console).Verify(c => c.Show("invalid mailbox address"));
    }

    [TestMethod]
    public void VerifyShowMailboxOnValidInputAddress()
    {
      Mock.Get(console).Setup(c => c.ReadLine("Enter mailbox address:")).Returns("mbx");
      Mock.Get(addressBook).Setup(a => a.GetMailbox("mbx")).Returns(mbx);
      var listAction = new ListMailboxInteraction(console, addressBook);
      listAction.ListMailbox();
      Mock.Get(console).Verify(c => c.Show("Mailbox mbx is empty"));
    }

    [TestMethod]
    public void ShowMessagesByType()
    {
      Mock.Get(console).Setup(c => c.ReadLine("Enter mailbox address:")).Returns("mbx");
      Mock.Get(addressBook).Setup(a => a.GetMailbox("mbx")).Returns(mbx);

      mbx.Add(email);
      mbx.Add(text);
      mbx.Add(voice);

      var listAction = new ListMailboxInteraction(console, addressBook);
      listAction.ListMailbox();

      Mock.Get(console).Verify(c => c.Show("Content of mailbox mbx:"));
      Mock.Get(console).Verify(c => c.Show("1: Email: subj"));
      Mock.Get(console).Verify(c => c.Show("2: Text: content"));
      Mock.Get(console).Verify(c => c.Show("3: Voice: from sender"));
    }
  }
}