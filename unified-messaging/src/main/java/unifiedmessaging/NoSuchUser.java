package unifiedmessaging;

@SuppressWarnings("serial")
public class NoSuchUser extends RuntimeException {

	public final String username;

	public NoSuchUser(String username) {
		this.username = username;
	}

}
