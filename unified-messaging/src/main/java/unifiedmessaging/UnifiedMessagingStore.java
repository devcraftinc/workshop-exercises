package unifiedmessaging;

public interface UnifiedMessagingStore {

	public abstract static class MessageInfo {
		public final String sender;
		public final String receiver;

		public MessageInfo(String sender, String receiver) {
			this.sender = sender;
			this.receiver = receiver;
		}
	}

	public static class PhotoMessageInfo extends MessageInfo {
		public final String caption;
		public final String photoHandle;

		public PhotoMessageInfo(String sender, String receiver, String caption, String photoHandle) {
			super(sender, receiver);
			this.caption = caption;
			this.photoHandle = photoHandle;
		}
	}

	public static class AudioMessageInfo extends MessageInfo {
		public final String audioHandle;

		public AudioMessageInfo(String sender, String receiver, String audioHandle) {
			super(sender, receiver);
			this.audioHandle = audioHandle;
		}

	}

	public static class LinkMessageInfo extends MessageInfo {
		public final String url;

		public LinkMessageInfo(String sender, String receiver, String url) {
			super(sender, receiver);
			this.url = url;
		}
	}

	public static class TextMessageInfo extends MessageInfo {

		public final String text;

		public TextMessageInfo(String sender, String receiver, String text) {
			super(sender, receiver);
			this.text = text;
		}
	}

	public static class User {
		public final String username;
		public final String password;

		public User(String username, String password) {
			this.username = username;
			this.password = password;
		}
	}

	void addUser(User user) throws UsernameAlreadyTaken;

	User getUser(String username);

	String getOrCreateSession(String username) throws NoSuchUser;

	User getActiveUser(String senderToken);

	void addMessage(MessageInfo message) throws NoSuchUser;

	MessageInfo[] retreiveMessagesForUser(String username);

	MessageInfo[] retreiveAllMessages();

}