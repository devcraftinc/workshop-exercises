package unifiedmessaging;

import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import unifiedmessaging.UnifiedMessagingStore.AudioMessageInfo;
import unifiedmessaging.UnifiedMessagingStore.LinkMessageInfo;
import unifiedmessaging.UnifiedMessagingStore.MessageInfo;
import unifiedmessaging.UnifiedMessagingStore.PhotoMessageInfo;
import unifiedmessaging.UnifiedMessagingStore.TextMessageInfo;
import unifiedmessaging.UnifiedMessagingStore.User;

class UnifiedMessagingImpl implements UnifiedMessaging {

	private UnifiedMessagingStore store;
	private FileStore fileStore;

	public UnifiedMessagingImpl(UnifiedMessagingStore store, FileStore fileStore) {
		this.store = store;
		this.fileStore = fileStore;
	}

	public void registerNewUser(String username, String password) throws UsernameAlreadyTaken {
		store.addUser(new User(username, password));
	}

	@Override
	public String login(String username, String password) throws InvalidCredentials {
		User user = store.getUser(username);
		if (user == null || !user.password.equals(password))
			throw new InvalidCredentials();
		try {
			return store.getOrCreateSession(username);
		} catch (NoSuchUser e) {
			// user was removed by another thread.
			throw new InvalidCredentials();
		}
	}

	@Override
	public void sendText(String senderToken, String receiverUsername, String text) throws NoSuchUser, InvalidToken {
		User sender = verifyActiveUser(senderToken);
		UnifiedMessagingStore.User receiver = verifyUserExists(receiverUsername);
		UnifiedMessagingStore.TextMessageInfo message = new UnifiedMessagingStore.TextMessageInfo(sender.username,
				receiver.username, text);
		store.addMessage(message);
	}

	@Override
	public void sendLink(String senderToken, String receiverUsername, String url) throws NoSuchUser, InvalidToken {
		User sender = verifyActiveUser(senderToken);
		UnifiedMessagingStore.User receiver = verifyUserExists(receiverUsername);
		UnifiedMessagingStore.LinkMessageInfo message = new UnifiedMessagingStore.LinkMessageInfo(sender.username,
				receiver.username, url);
		store.addMessage(message);
	}

	private UnifiedMessagingStore.User verifyUserExists(String username) throws NoSuchUser {
		UnifiedMessagingStore.User user = store.getUser(username);
		if (user == null) {
			throw new NoSuchUser(username);
		}
		return user;
	}

	private User verifyActiveUser(String token) throws InvalidToken {
		User user = store.getActiveUser(token);
		if (user == null) {
			throw new InvalidToken();
		}
		return user;
	}

	@Override
	public String sendVoice(String senderToken, String receiverUsername, byte[] bytes) throws InvalidToken, NoSuchUser {
		User sender = verifyActiveUser(senderToken);
		UnifiedMessagingStore.User receiver = verifyUserExists(receiverUsername);
		String path = fileStore.store(bytes);
		UnifiedMessagingStore.AudioMessageInfo message = new UnifiedMessagingStore.AudioMessageInfo(sender.username,
				receiver.username, path);
		store.addMessage(message);
		return path;
	}

	@Override
	public String sendPhoto(String senderToken, String receiverUsername, String caption, byte[] bytes)
			throws NoSuchUser, InvalidToken {
		User sender = verifyActiveUser(senderToken);
		UnifiedMessagingStore.User receiver = verifyUserExists(receiverUsername);
		String path = fileStore.store(bytes);
		UnifiedMessagingStore.PhotoMessageInfo message = new UnifiedMessagingStore.PhotoMessageInfo(sender.username,
				receiver.username, caption, path);
		store.addMessage(message);
		return path;
	}

	@Override
	public byte[] retreiveFile(String activeToken, String filename) {
		verifyActiveUser(activeToken);
		return fileStore.retreive(filename);
	}

	@Override
	public MessageInfo[] listMessages(String token) {
		User sender = verifyActiveUser(token);
		return store.retreiveMessagesForUser(sender.username);
	}

	@Override
	public String[] suggestFriends(String token) {
		User user = verifyActiveUser(token);

		Set<String> suggestion = new LinkedHashSet<>();
		MessageInfo[] myMessages = store.retreiveMessagesForUser(user.username);
		MessageInfo[] allMessages = store.retreiveAllMessages();
		for (int i = 0; i < allMessages.length; i++) {
			MessageInfo m = allMessages[i];
			if (m instanceof TextMessageInfo) {
				TextMessageInfo textMessage = (TextMessageInfo) m;
				if (textMessage.text.contains(user.username)) {
					suggestion.add(textMessage.sender);
					suggestion.add(textMessage.receiver);
				}
			} else if (m instanceof LinkMessageInfo) {
				LinkMessageInfo linkMessage = (LinkMessageInfo) m;
				if (pageText(linkMessage.url).contains(user.username)) {
					suggestion.add(linkMessage.sender);
					suggestion.add(linkMessage.receiver);
				}
			} else if (m instanceof PhotoMessageInfo) {
				PhotoMessageInfo photoMessage = (PhotoMessageInfo) m;
				if (photoMessage.caption.contains(user.username)) {
					suggestion.add(photoMessage.sender);
					suggestion.add(photoMessage.receiver);
				}

				byte[] photo = fileStore.retreive(photoMessage.photoHandle);
				if (isInPhoto(user.username, photo)) {
					suggestion.add(photoMessage.sender);
					suggestion.add(photoMessage.receiver);
				}
			} else if (m instanceof AudioMessageInfo) {
				AudioMessageInfo audioMessage = (AudioMessageInfo) m;
				byte[] audio = fileStore.retreive(audioMessage.audioHandle);
				if (isMentionedInAudio(user.username, audio)) {
					suggestion.add(audioMessage.sender);
					suggestion.add(audioMessage.receiver);
				}
			}
		}

		Set<String> myCurrentFriends = getAllUsers(myMessages);
		suggestion.removeAll(myCurrentFriends);
		return suggestion.toArray(new String[suggestion.size()]);
	}

	private boolean isMentionedInAudio(String username, byte[] audio) {
		return new String(audio).contains(username);
	}

	private boolean isInPhoto(String username, byte[] photo) {
		return new String(photo).contains(username);
	}

	private String pageText(String url) {
		return url;
	}

	private Set<String> getAllUsers(MessageInfo[] messages) {
		Set<String> result = new LinkedHashSet<>();
		for (int i = 0; i < messages.length; i++) {
			MessageInfo m = messages[i];
			result.add(m.sender);
			result.add(m.receiver);
		}
		return result;
	}

	@Override
	public MessageInfo[] findInappropriateContent(String adminToken) {
		User user = verifyActiveUser(adminToken);
		verifyUserIsAdmin(user);
		List<MessageInfo> inappropriate = new LinkedList<>();
		MessageInfo[] allMessages = store.retreiveAllMessages();
		for (int i = 0; i < allMessages.length; i++) {
			MessageInfo m = allMessages[i];
			if (m instanceof TextMessageInfo) {
				TextMessageInfo message = (TextMessageInfo) m;
				if (isInappropriate(message.text)) {
					inappropriate.add(message);
				}
			} else if (m instanceof LinkMessageInfo) {
				LinkMessageInfo message = (LinkMessageInfo) m;
				if (isInappropriate(pageText(message.url))) {
					inappropriate.add(message);
				}
			} else if (m instanceof PhotoMessageInfo) {
				PhotoMessageInfo message = (PhotoMessageInfo) m;
				if (isInappropriate(message.caption)) {
					inappropriate.add(message);
				} else {
					byte[] photo = fileStore.retreive(message.photoHandle);
					if (isInappropriate(photo)) {
						inappropriate.add(message);
					}
				}
			} else if (m instanceof AudioMessageInfo) {
				AudioMessageInfo message = (AudioMessageInfo) m;
				byte[] audio = fileStore.retreive(message.audioHandle);
				if (isInappropriate(text2Speech(audio))) {
					inappropriate.add(message);
				}
			}
		}
		return inappropriate.toArray(new MessageInfo[inappropriate.size()]);
	}

	private String text2Speech(byte[] audio) {
		return new String(audio);
	}

	private boolean isInappropriate(byte[] photo) {
		return isInappropriate(new String(photo));
	}

	private boolean isInappropriate(String text) {
		String[] words = text.split("\\s+");
		for (int i = 0; i < words.length; i++) {
			if (isFWord(words[i]))
				return true;
		}
		return false;
	}

	private boolean isFWord(String word) {
		return word.startsWith("f");
	}

	private void verifyUserIsAdmin(User user) {
		if (!user.username.equalsIgnoreCase("admin")) {
			throw new NoPermission();
		}
	}

}