package unifiedmessaging;

public interface FileStore {
	public String store(byte[] bytes);
	public byte[] retreive(String storePath);
}