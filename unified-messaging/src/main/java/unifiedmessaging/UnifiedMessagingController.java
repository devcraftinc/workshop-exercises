package unifiedmessaging;

import java.io.IOException;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import unifiedmessaging.UnifiedMessagingStore.MessageInfo;

@RestController
class UnifiedMessagingController {
	@Autowired
	private UnifiedMessaging unifiedmessaging;

	@PostMapping(path = "/registration")
	@ResponseStatus(HttpStatus.CREATED)
	public void registerNewUser(@RequestBody Map<String, String> map) throws UsernameAlreadyTaken {
		String username = map.get("username");
		String password = map.get("password");
		unifiedmessaging.registerNewUser(username, password);
	}

	@PostMapping(path = "/login")
	@ResponseStatus(HttpStatus.OK)
	public String login(@RequestBody Map<String, String> map) throws InvalidCredentials {
		String username = map.get("username");
		String password = map.get("password");
		String token = unifiedmessaging.login(username, password);
		System.out.println(token); //
		return token;
	}

	@PostMapping(path = "/messages/text")
	@ResponseStatus(HttpStatus.OK)
	public void sendText(@RequestBody Map<String, String> map) throws NoSuchUser, InvalidToken {
		String activeToken = map.get("active_token");
		String targetUser = map.get("target_user");
		String text = map.get("text");
		unifiedmessaging.sendText(activeToken, targetUser, text);
	}

	@PostMapping(path = "/messages/link")
	@ResponseStatus(HttpStatus.OK)
	public void sendLink(@RequestBody Map<String, String> map) throws NoSuchUser, InvalidToken {
		String activeToken = map.get("active_token");
		String targetUser = map.get("target_user");
		String url = map.get("url");
		unifiedmessaging.sendLink(activeToken, targetUser, url);
	}

	@PostMapping("/messages/voice")
	public String sendVoice(@RequestParam("file") MultipartFile voiceFile, @RequestBody Map<String, String> map)
			throws IOException, InvalidToken, NoSuchUser {
		String activeToken = map.get("active_token");
		String targetUser = map.get("target_user");
		return unifiedmessaging.sendVoice(activeToken, targetUser, voiceFile.getBytes());
	}

	@PostMapping("/messages/photo")
	public String sendPhoto(@RequestParam("file") MultipartFile photoFile, @RequestBody Map<String, String> map)
			throws IOException, InvalidToken, NoSuchUser {
		String activeToken = map.get("active_token");
		String targetUser = map.get("target_user");
		String caption = map.get("caption");
		return unifiedmessaging.sendPhoto(activeToken, targetUser, caption, photoFile.getBytes());
	}

	@GetMapping("/files/{filename}")
	public byte[] serveFile(@PathVariable("filename") String filename, @RequestParam("token") String token) {
		return unifiedmessaging.retreiveFile(token, filename);
	}

	@GetMapping("/messages")
	public MessageInfo[] serveFile(@RequestParam("token") String token) {
		return unifiedmessaging.listMessages(token);
	}

	@GetMapping("/suggested_freinds")
	public String[] suggestFriends(@RequestParam("token") String token) {
		return unifiedmessaging.suggestFriends(token);
	}

	@GetMapping("/messages/inappropriate")
	public MessageInfo[] inappropriateMessages(@RequestParam("token") String token) {
		return unifiedmessaging.findInappropriateContent(token);
	}

	@ControllerAdvice
	public static class GlobalControllerExceptionHandler {
		@ExceptionHandler(UsernameAlreadyTaken.class)
		public ResponseEntity<String> handleConflict(UsernameAlreadyTaken e) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("UsernameAlreadyTaken");
		}

		@ExceptionHandler(InvalidCredentials.class)
		public ResponseEntity<String> handleInvalidCredentials(InvalidCredentials e) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("InvalidCredentials");
		}

		@ExceptionHandler(NoSuchUser.class)
		public ResponseEntity<String> noSuchUser(NoSuchUser e) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("NoSuchUser");
		}

		@ExceptionHandler(InvalidToken.class)
		public ResponseEntity<String> invalidToken(InvalidToken e) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("InvalidToken");
		}

		@ExceptionHandler(IOException.class)
		public ResponseEntity<String> ioFailure(IOException e) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal server error");
		}

		@ExceptionHandler(NoPermission.class)
		public ResponseEntity<String> noSuchUser(NoPermission e) {
			return ResponseEntity.status(HttpStatus.FORBIDDEN).body("NoPermission");
		}

	}

}