package unifiedmessaging;

import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class MessagingIntegrationTests extends MessagingSpec {

	@TestConfiguration
	public static class TestConfig {

		@Bean
		TestApi testApi(final UnifiedMessagingStoreImpl store) {
			return new TestApi() {
				@Override
				public void clear() {
					store.clear();
				}
			};
		}
	}

	private UnifiedMessaging unifiedMessaging;

	@Autowired
	private MockMvc mvc;

	@Autowired
	private TestApi testApi;

	@Before
	public void init() {
		unifiedMessaging = new MockMvcTestClient(mvc);
		testApi.clear();
	}

	@Override
	protected UnifiedMessaging unifiedMessaging() {
		return unifiedMessaging;
	}
}