package unifiedmessaging;

import java.util.HashMap;
import java.util.UUID;

import org.junit.Before;

public class MessagingUnitTests extends MessagingSpec {

	private UnifiedMessagingStoreImpl store = new UnifiedMessagingStoreImpl();
	private UnifiedMessaging unifiedMessaging;
	
	private FileStore fileStore = new FileStore() {
		private HashMap<String, byte[]> files = new HashMap<>();

		@Override
		public String store(byte[] bytes) {
			String key = UUID.randomUUID().toString();
			files.put(key, bytes);
			return key;
		}

		@Override
		public byte[] retreive(String storePath) {
			return files.get(storePath);
		}
	};

	@Before
	public void init() {
		unifiedMessaging = new UnifiedMessagingImpl(store,fileStore);
	}

	@Override
	protected UnifiedMessaging unifiedMessaging() {
		return unifiedMessaging;
	}
}