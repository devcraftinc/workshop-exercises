package unifiedmessaging;

import static java.util.Arrays.*;
import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;

import unifiedmessaging.UnifiedMessagingStore.AudioMessageInfo;
import unifiedmessaging.UnifiedMessagingStore.MessageInfo;
import unifiedmessaging.UnifiedMessagingStore.PhotoMessageInfo;
import unifiedmessaging.UnifiedMessagingStore.TextMessageInfo;

public abstract class MessagingSpec {

	public MessagingSpec() {
	}

	protected abstract UnifiedMessaging unifiedMessaging();

	@Test(expected = UsernameAlreadyTaken.class)
	public void shouldRejectRegistrationWithExistingUsername() throws Exception {
		unifiedMessaging().registerNewUser("some user", "some password");
		unifiedMessaging().registerNewUser("some user", "some other password");
	}

	@Test(expected = InvalidCredentials.class)
	public void shouldRejectLoginWithInvalidUsername() throws Exception {
		unifiedMessaging().login("some user", "some password");
	}

	@Test(expected = InvalidCredentials.class)
	public void shouldRejectLoginWithInvalidPassword() throws Exception {
		unifiedMessaging().registerNewUser("some user", "some password");
		unifiedMessaging().login("some user", "some other password");
	}
	
	@Test(expected = InvalidToken.class)
	public void shouldRejectSendMessageWithInvalidToken() throws Exception {
		unifiedMessaging().registerNewUser("some user", "some password");
		unifiedMessaging().sendText("bad token", "some user", "some text");
	}

	@Test(expected = NoSuchUser.class)
	public void shouldRejectSendMessageToInvalidUser() throws Exception {
		String token = registerAndLogin("sender", "some password");
		unifiedMessaging().sendText(token, "no such user", "some text");
	}

	@Test
	public void aRegisteredUserCanLogin() throws Exception {
		unifiedMessaging().registerNewUser("some user", "some password");
		assertNotNull(unifiedMessaging().login("some user", "some password"));
	}

	@Test
	public void secondLoginShouldReturnSameToken() throws Exception {
		String token1 = registerAndLogin("some user", "some password");
		String token2 = unifiedMessaging().login("some user", "some password");
		assertEquals(token1, token2);
	}

	@Test
	public void differentUsersLoginsShouldReturnDifferentTokens() throws Exception {
		String token1 = registerAndLogin("user1", "password1");
		String token2 = registerAndLogin("user2", "password2");
		assertNotEquals(token1, token2);
	}

	@Test
	public void activeUserCanSendTextMessage() throws Exception {
		unifiedMessaging().registerNewUser("user2", "password2");
		String token = registerAndLogin("user1", "password1");
		unifiedMessaging().sendText(token, "user2", "some text");

		TextMessageInfo m = (TextMessageInfo) unifiedMessaging().listMessages(token)[0];
		assertEquals("some text", m.text);
	}

	@Test
	public void activeUserCanSendLink() throws Exception {
		unifiedMessaging().registerNewUser("user2", "password2");
		String token = registerAndLogin("user1", "password1");
		unifiedMessaging().sendLink(token, "user2", "http://www.devcraft.pro");
	}

	@Test
	public void bothSenderAndReceiverCanSeeTheMessage() throws Exception {
		String token1 = registerAndLogin("user1", "password1");
		String token2 = registerAndLogin("user2", "password1");
		unifiedMessaging().sendText(token1, "user2", "some text");
		unifiedMessaging().sendLink(token1, "user2", "http://www.devcraft.pro");
		unifiedMessaging().sendVoice(token1, "user2", "hi there".getBytes());
		unifiedMessaging().sendPhoto(token1, "user2", "caption", "flower".getBytes());

		assertEquals(4, unifiedMessaging().listMessages(token1).length);
		assertEquals(4, unifiedMessaging().listMessages(token2).length);
	}

	@Test
	public void activeUserCanSendVoice() throws Exception {
		unifiedMessaging().registerNewUser("user2", "password2");
		String token = registerAndLogin("user1", "password1");
		String path = unifiedMessaging().sendVoice(token, "user2", "hi there".getBytes());
		assertEquals("hi there", new String(unifiedMessaging().retreiveFile(token, path)));
		AudioMessageInfo m = (AudioMessageInfo) unifiedMessaging().listMessages(token)[0];
		assertEquals(path, m.audioHandle);
	}

	@Test
	public void activeUserCanSendPhoto() throws Exception {
		unifiedMessaging().registerNewUser("user2", "password2");
		String token = registerAndLogin("user1", "password1");
		String path = unifiedMessaging().sendPhoto(token, "user2", "caption", "flower".getBytes());
		assertEquals("flower", new String(unifiedMessaging().retreiveFile(token, path)));
		PhotoMessageInfo m = (PhotoMessageInfo) unifiedMessaging().listMessages(token)[0];
		assertEquals("caption", m.caption);
		assertEquals(path, m.photoHandle);
	}

	private String registerAndLogin(String username, String password) throws UsernameAlreadyTaken, InvalidCredentials {
		unifiedMessaging().registerNewUser(username, password);
		return unifiedMessaging().login(username, password);
	}

	@Test
	public void shouldSuggestOnlyUsersIDontAlreadyKnow() throws Exception {
		String token1 = registerAndLogin("user1", "password1");
		String token2 = registerAndLogin("user2", "password2");
		unifiedMessaging().registerNewUser("user3", "password3");
		unifiedMessaging().registerNewUser("user4", "password4");
		unifiedMessaging().registerNewUser("user5", "password4");
		unifiedMessaging().registerNewUser("user6", "password4");
		unifiedMessaging().registerNewUser("user7", "password4");

		unifiedMessaging().sendText(token1, "user2", "I know you");
		unifiedMessaging().sendText(token2, "user3", "user1 is awesome");
		unifiedMessaging().sendLink(token2, "user4", "http://www.user1.pro");
		unifiedMessaging().sendVoice(token2, "user5", "did you hear the news about user1".getBytes());
		unifiedMessaging().sendPhoto(token2, "user6", "picture of user1", "user1 holding flower".getBytes());
		unifiedMessaging().sendPhoto(token2, "user7", "picture of user2", "user2 holding flower".getBytes());

		List<String> suggested = asList(unifiedMessaging().suggestFriends(token1));
		assertTrue(suggested.containsAll(asList(new String[] { "user3", "user4", "user5", "user6" })));
		assertFalse(suggested.contains("user2"));
		assertFalse(suggested.contains("user7"));
	}

	@Test
	public void shouldFindInappropriateMessages() throws Exception {
		String admin = registerAndLogin("admin", "password1");
		String token1 = registerAndLogin("user1", "password1");
		registerAndLogin("user2", "password2");

		unifiedMessaging().sendText(token1, "user2", "f... u");
		unifiedMessaging().sendLink(token1, "user2", "http://www. f.... pro");
		unifiedMessaging().sendVoice(token1, "user2", "f".getBytes());
		unifiedMessaging().sendPhoto(token1, "user2", "f", "f".getBytes());

		unifiedMessaging().sendText(token1, "user2", "hi");
		unifiedMessaging().sendLink(token1, "user2", "http://www. hi .pro");
		unifiedMessaging().sendVoice(token1, "user2", "hi".getBytes());
		unifiedMessaging().sendPhoto(token1, "user2", "hi", "hi".getBytes());

		List<MessageInfo> suggested = asList(unifiedMessaging().findInappropriateContent(admin));
		assertEquals(4, suggested.size());
	}

	
	@Test (expected= NoPermission.class)
	public void onlyAdminCanFindInappropriateMessages() throws Exception {
		String token = registerAndLogin("not admin", "password1");
		unifiedMessaging().findInappropriateContent(token);
	}
}