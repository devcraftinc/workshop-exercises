package unifiedmessaging;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Iterator;

import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Assert;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import unifiedmessaging.UnifiedMessagingStore.AudioMessageInfo;
import unifiedmessaging.UnifiedMessagingStore.LinkMessageInfo;
import unifiedmessaging.UnifiedMessagingStore.MessageInfo;
import unifiedmessaging.UnifiedMessagingStore.PhotoMessageInfo;
import unifiedmessaging.UnifiedMessagingStore.TextMessageInfo;

public class MockMvcTestClient implements UnifiedMessaging {

	private MockMvc mvc;

	public MockMvcTestClient(MockMvc mock) {
		this.mvc = mock;
	}

	public void registerNewUser(String username, String password) throws UsernameAlreadyTaken {
		MvcResult mvcResult = null;
		try {
			mvcResult = this.mvc.perform(post("/registration").contentType(MediaType.APPLICATION_JSON)
					.content(toJsonString(username, password))).andReturn();
			if (mvcResult.getResponse().getStatus() == HttpStatus.CREATED.value()) {
				return;
			}
		} catch (Exception e) {
			Assert.fail("unexpected exception:" + e.getMessage());
		}

		throw handleErrorResponse(mvcResult);
	}

	public String login(String username, String password) throws InvalidCredentials {
		MvcResult mvcResult = null;
		try {
			mvcResult = this.mvc
					.perform(post("/login").contentType(MediaType.APPLICATION_JSON)
							.content(new JSONObject().put("username", username).put("password", password).toString()))
					.andReturn();
			if (mvcResult.getResponse().getStatus() == HttpStatus.OK.value()) {
				return mvcResult.getResponse().getContentAsString();
			}

		} catch (Exception e) {
			Assert.fail("unexpected exception:" + e.getMessage());
		}
		throw handleErrorResponse(mvcResult);
	}

	@Override
	public void sendText(String senderToken, String receiverUsername, String text) throws NoSuchUser, InvalidToken {
		MvcResult mvcResult = null;
		try {
			mvcResult = this.mvc
					.perform(
							post("/messages/text").contentType(MediaType.APPLICATION_JSON)
									.content(new JSONObject().put("active_token", senderToken)
											.put("target_user", receiverUsername).put("text", text).toString()))
					.andReturn();
			if (mvcResult.getResponse().getStatus() == HttpStatus.OK.value()) {
				return;
			}
		} catch (Exception e) {
			Assert.fail("unexpected exception:" + e.getMessage());
		}

		throw handleErrorResponse(mvcResult);
	}

	@Override
	public void sendLink(String senderToken, String receiverUsername, String url) throws NoSuchUser {
		MvcResult mvcResult = null;
		try {
			JSONObject content = new JSONObject().put("active_token", senderToken).put("target_user", receiverUsername)
					.put("url", url);
			mvcResult = this.mvc
					.perform(post("/messages/link").contentType(MediaType.APPLICATION_JSON).content(content.toString()))
					.andReturn();
			if (mvcResult.getResponse().getStatus() == HttpStatus.OK.value()) {
				return;
			}
		} catch (Exception e) {
			Assert.fail("unexpected exception:" + e.getMessage());
		}

		throw handleErrorResponse(mvcResult);

	}

	@Override
	public String sendVoice(String senderToken, String receiverUsername, byte[] bytes) throws NoSuchUser {
		MockMultipartFile multipartFile = new MockMultipartFile("file", "test.txt", "text/plain", bytes);
		MvcResult mvcResult = null;
		try {
			mvcResult = this.mvc.perform(multipart("/messages/voice").file(multipartFile)
					.contentType(MediaType.APPLICATION_JSON).content(new JSONObject().put("active_token", senderToken)
							.put("target_user", receiverUsername).toString()))
					.andReturn();
			if (mvcResult.getResponse().getStatus() == HttpStatus.OK.value()) {
				return mvcResult.getResponse().getContentAsString();
			}

		} catch (Exception e) {
			Assert.fail("unexpected exception:" + e.getMessage());
		}

		throw handleErrorResponse(mvcResult);
	}

	@Override
	public String sendPhoto(String senderToken, String receiverUsername, String caption, byte[] bytes)
			throws NoSuchUser, InvalidToken {
		MockMultipartFile multipartFile = new MockMultipartFile("file", "test.txt", "text/plain", bytes);
		MvcResult mvcResult = null;
		try {
			mvcResult = this.mvc
					.perform(multipart("/messages/photo").file(multipartFile).contentType(MediaType.APPLICATION_JSON)
							.content(new JSONObject().put("active_token", senderToken)
									.put("target_user", receiverUsername).put("caption", caption).toString()))
					.andReturn();
			if (mvcResult.getResponse().getStatus() == HttpStatus.OK.value()) {
				return mvcResult.getResponse().getContentAsString();
			}
		} catch (Exception e) {
			Assert.fail("unexpected exception:" + e.getMessage());
		}

		throw handleErrorResponse(mvcResult);
	}

	private String toJsonString(String username, String password) {
		try {
			return new JSONObject().put("username", username).put("password", password).toString();
		} catch (JSONException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public byte[] retreiveFile(String activeToken, String filename) {
		MvcResult mvcResult = null;
		try {
			mvcResult = this.mvc.perform(
					get("/files" + "/" + filename + "?token=" + activeToken).contentType(MediaType.APPLICATION_JSON))
					.andReturn();
			if (mvcResult.getResponse().getStatus() == HttpStatus.OK.value()) {
				return mvcResult.getResponse().getContentAsByteArray();
			}
		} catch (Exception e) {
			Assert.fail("unexpected exception:" + e.getMessage());
		}

		throw handleErrorResponse(mvcResult);
	}

	@Override
	public MessageInfo[] listMessages(String token) {
		MvcResult mvcResult = null;
		try {
			mvcResult = this.mvc.perform(get("/messages" + "?token=" + token).contentType(MediaType.APPLICATION_JSON))
					.andReturn();
			if (mvcResult.getResponse().getStatus() == HttpStatus.OK.value()) {
				String contentAsString = mvcResult.getResponse().getContentAsString();
				return toMessages(contentAsString);
			}
		} catch (Exception e) {
			Assert.fail("unexpected exception:" + e.getMessage());
		}

		throw handleErrorResponse(mvcResult);
	}

	private MessageInfo[] toMessages(JsonArray jArray) {
		ArrayList<MessageInfo> result = new ArrayList<>();
		Iterator<JsonElement> i = jArray.iterator();
		while (i.hasNext()) {
			JsonObject o = (JsonObject)i.next();
			if (o.has("text")) {
				result.add(new TextMessageInfo(o.get("sender").getAsString(), o.get("receiver").getAsString(),
						o.get("text").getAsString()));
			} else if (o.has("url")) {
				result.add(new LinkMessageInfo(o.get("sender").getAsString(), o.get("receiver").getAsString(),
						o.get("url").getAsString()));
			} else if (o.has("caption")) {
				result.add(new PhotoMessageInfo(o.get("sender").getAsString(), o.get("receiver").getAsString(),
						o.get("caption").getAsString(), o.get("photoHandle").getAsString()));
			} else {
				result.add(new AudioMessageInfo(o.get("sender").getAsString(), o.get("receiver").getAsString(),
						o.get("audioHandle").getAsString()));

			}
		}
		return result.toArray(new MessageInfo[result.size()]);
	}

	private RuntimeException handleErrorResponse(MvcResult mvcResult) {
		try {
			if (mvcResult.getResponse().getStatus() == HttpStatus.BAD_REQUEST.value()
					&& mvcResult.getResponse().getContentAsString().equals("InvalidToken")) {
				return new InvalidToken();
			}
			if (mvcResult.getResponse().getStatus() == HttpStatus.BAD_REQUEST.value()
					&& mvcResult.getResponse().getContentAsString().equals("NoSuchUser")) {
				return new NoSuchUser("");
			}
			if (mvcResult.getResponse().getStatus() == HttpStatus.BAD_REQUEST.value()
					&& mvcResult.getResponse().getContentAsString().equals("InvalidCredentials")) {
				return new InvalidCredentials();
			}
			if (mvcResult.getResponse().getStatus() == HttpStatus.BAD_REQUEST.value()
					&& mvcResult.getResponse().getContentAsString().equals("UsernameAlreadyTaken")) {
				return new UsernameAlreadyTaken();
			}
			if (mvcResult.getResponse().getStatus() == HttpStatus.FORBIDDEN.value()
					&& mvcResult.getResponse().getContentAsString().equals("NoPermission")) {
				return new NoPermission();
			}
		} catch (UnsupportedEncodingException e) {
			return new RuntimeException(e);
		}

		return new RuntimeException("unexpected http response");
	}

	@Override
	public String[] suggestFriends(String token) {
		MvcResult mvcResult = null;
		try {
			mvcResult = this.mvc
					.perform(get("/suggested_freinds" + "?token=" + token).contentType(MediaType.APPLICATION_JSON))
					.andReturn();
			if (mvcResult.getResponse().getStatus() == HttpStatus.OK.value()) {
				String contentAsString = mvcResult.getResponse().getContentAsString();
				JsonParser jsonParser = new JsonParser();
				JsonArray jArray = (JsonArray) jsonParser.parse(contentAsString);
				return toStrings(jArray);
			}
		} catch (Exception e) {
			Assert.fail("unexpected exception:" + e.getMessage());
		}

		throw handleErrorResponse(mvcResult);
	}

	private String[] toStrings(JsonArray jArray) {
		ArrayList<String> result = new ArrayList<>();
		Iterator<JsonElement> i = jArray.iterator();
		while (i.hasNext()) {
			JsonElement e = (JsonElement) i.next();
			result.add(e.getAsString());
		}
		return result.toArray(new String[result.size()]);
	}

	@Override
	public MessageInfo[] findInappropriateContent(String adminToken) {
		MvcResult mvcResult = null;
		try {
			mvcResult = this.mvc.perform(get("/messages/inappropriate" + "?token=" + adminToken).contentType(MediaType.APPLICATION_JSON))
					.andReturn();
			if (mvcResult.getResponse().getStatus() == HttpStatus.OK.value()) {
				String contentAsString = mvcResult.getResponse().getContentAsString();
				return toMessages(contentAsString);
			}
		} catch (Exception e) {
			Assert.fail("unexpected exception:" + e.getMessage());
		}

		throw handleErrorResponse(mvcResult);
	}

	private MessageInfo[] toMessages(String contentAsString) {
		JsonParser jsonParser = new JsonParser();
		JsonArray jArray = (JsonArray) jsonParser.parse(contentAsString);
		return toMessages(jArray);
	}

}
